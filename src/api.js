import axios from "axios";
const baseUrl = "http://localhost:3001/";

export async function getUsers() {
  const resp = await axios.get(baseUrl + "users");
  return resp.data;
}
