import React, { useState, useEffect } from "react";
import { getUsers } from "./api.js";

function App() {
  const a = async () => {
    const newUsers = await getUsers();
    setUsers(newUsers);
  };
  useEffect(() => {
    a();
  }, []);

  const [users, setUsers] = useState([]);
  return (
    <div className="App" style={{ marginLeft: "1rem" }}>
      <h1>Fake data:</h1>
      {users.map(u => (
        <li key={u.id}>
          {u.firstName} {u.lastName}
        </li>
      ))}
    </div>
  );
}

export default App;
