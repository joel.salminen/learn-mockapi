// http://marak.github.io/faker.js/#toc7__anchor
// https://json-schema-faker.js.org/#gist/4199ca90fb5cd05337824b0695d17b5e

module.exports = {
  type: "object",
  properties: {
    users: {
      type: "array",
      minItems: 5,
      maxItems: 10,
      items: {
        type: "object",
        properties: {
          id: {
            type: "integer",
            unique: true,
            minimum: 1
          },
          firstName: {
            type: "string",
            faker: "name.findName"
          },
          lastName: {
            type: "string",
            faker: "name.lastName"
          },
          email: {
            type: "string",
            faker: "internet.email"
          }
        },
        required: ["id", "firstName", "lastName", "email"]
      }
    }
  },
  required: ["users"]
};
